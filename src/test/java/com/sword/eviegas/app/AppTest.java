package com.sword.eviegas.app;


import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.cli.ParseException;

import java.io.IOException;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
    /**
     * Create the test case
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    /**
     * Test for main class
     */
    public void testApp() throws IOException, ParseException {
    	String[] args = {
    			"--redis.url", "localhost:6379",
    			"--redis.catalog", "geomesa"
    	};
    	
    	new App();
    	App.main(args);
        assertTrue(true);
    }
}
