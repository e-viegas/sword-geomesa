package com.sword.eviegas.data;

import java.io.IOException;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for any kind of data sets.
 */
public class DataSetTest extends TestCase {
    /**
     * Create the test case
     * @param testName name of the test case
     */
    public DataSetTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(DataSetTest.class);
    }

    /**
     * Test to get the data
     * @throws IOException 
     */
    public void testGetData() throws IOException {
    	DataSet data = new CSVDataSet(
    	        "src/main/resources/test.csv", true,
                false, 4
        );

    	data.load();
    	assertEquals(
    	        data.getType(),
                data.getData(0).getType().getTypeName()
        );
    	assertNull(data.getData(data.size()));
    }
    
    /**
     * Test to get the size
     * @throws IOException 
     */
    public void testSize() throws IOException {
    	DataSet data = new CSVDataSet(
    	        "src/main/resources/test.csv", true,
                false, 4
        );
    	assertEquals(-1, data.size());	
    	
    	data.load();
    	assertEquals(5, data.size());
    	
    	data = new CSVDataSet(
    	        "src/main/resources/test.csv", true,
                true, 1);
    	data.load();
    	assertEquals(4, data.size());
    }
    
    /**
     * Check if the data set is read-only or not
     */
    public void testIsReadonly() {
    	DataSet ds1 = new CSVDataSet(
    	        "src/main/resources/test.csv", true,
                false, 4
        );
    	assertTrue(ds1.isReadonly());
    	
    	DataSet ds2 = new CSVDataSet(
    	        "src/main/resources/test.csv", false,
                false, 4
        );
    	assertFalse(ds2.isReadonly());
    }
}
