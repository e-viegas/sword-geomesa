package com.sword.eviegas.data;

import java.io.IOException;

import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for any kind of data sets.
 */
public class CSVDataSetTest extends TestCase {
    /**
     * Create the test case
     * @param testName name of the test case
     */
    public CSVDataSetTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(CSVDataSetTest.class);
    }

    /**
     * Test to get the data type
     */
    public void testGetType() {
    	DataSet ds1 = new CSVDataSet(
    			"src/main/resources/test.csv", true,
				false, 4
		);
    	assertTrue(ds1.getType().contains("CSV-file"));
    	
    	DataSet ds2 = new CSVDataSet(
    			"test.csv", true,
				false, 4, "test"
		);
    	assertTrue(ds2.getType().contains("CSV-test"));
    }
    
    /**
     * Test to get the simple feature type
     * @throws IOException probably
     */
    public void testGetSft() throws IOException {
    	DataSet dataset = new CSVDataSet(
    			"src/main/resources/test.csv", true,
				false, 1
		);

    	SimpleFeatureType sft1 = dataset.getSimpleFeatureType();
    	SimpleFeatureType sft2 = dataset.getSimpleFeatureType();
    	assertEquals(sft1, sft2);
    }
    
    /**
     * Test to load data from the CSv file
     * @throws IOException probably
     */
    public void testLoad() throws IOException {
    	DataSet ds1 = new CSVDataSet(
    			"src/main/resources/test.csv", true,
				false, 4
		);
    	assertNull(ds1.getData(0));
    	
    	ds1.load();
    	assertNotNull(ds1.getData(0));
    	
    	SimpleFeature ft = ds1.getData(0);
    	ds1.load();
    	assertEquals(ft, ds1.getData(0));
    	
    	DataSet ds2 = new CSVDataSet(
    			"src/main/resources/error.csv", true,
				false, 4
		);
    	ds2.load();
    	assertNotNull(ds2.getData(0));
    	
    	DataSet ds3 = new CSVDataSet(
    			"src/main/resources/test.csv", true,
				true, 4
		);
    	ds3.load();
    	assertEquals(4, ds3.size());
    }
    
    /**
     * Test to read the header and the fields
     * @throws IOException probably
     */
    public void testGetFields() throws IOException {
    	CSVDataSet ds1 = new CSVDataSet(
    			"src/main/resources/test.csv", true,
				false, 4
		);
    	ds1.getSimpleFeatureType();
    	assertEquals(5, ds1.getFields().size());
    	
    	CSVDataSet ds2 = new CSVDataSet(
    			"src/main/resources/test.csv", true,
				true, 4
		);
    	ds2.getSimpleFeatureType();
    	assertEquals(5, ds2.getFields().size());
    }
}
