package com.sword.eviegas.geomesa;

import com.sword.eviegas.data.CSVDataSet;
import com.sword.eviegas.data.DataSet;
import org.apache.commons.cli.ParseException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.geotools.data.DataStore;
import org.geotools.data.Query;
import org.geotools.filter.text.cql2.CQLException;
import org.geotools.filter.text.ecql.ECQL;
import org.opengis.feature.simple.SimpleFeature;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.List;

/**
 * Unit test for any kind of data sets.
 */
public class AccumuloGeoMesaToolBoxTest extends TestCase {
	/**
	 * 
	 */
	private String[] args;
	
    /**
     * Create the test case
     * @param testName name of the test case
     */
    public AccumuloGeoMesaToolBoxTest(String testName) {
        super(testName);
        this.args = new String[12];
        
        this.args[0] = "--accumulo.instance.id";
        this.args[1] = "localhost:22";
        this.args[2] = "--accumulo.zookeepers";
        this.args[3] = "localhost:2181";
        this.args[4] = "--accumulo.user";
        this.args[5] = "root";
        this.args[6] = "--accumulo.catalog";
        this.args[7] = "geomesa";
        this.args[8] = "--geomesa.security.visibilities";
        this.args[9] = "user";
        this.args[10] = "--geomesa.security.auths";
        this.args[11] = "user";
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AccumuloGeoMesaToolBoxTest.class);
    }

    /**
     * Test to get a tool box for Accumulo 
     * @throws ParseException probably
     */
    public void testGet() throws ParseException {
    	GeoMesaToolBox tools_1 = AccumuloGeoMesaToolBox.get(this.args);
    	GeoMesaToolBox tools_2 = AccumuloGeoMesaToolBox.get(this.args);
    	assertEquals(tools_1, tools_2);
    }
    
    /**
     * Test to add whatever data set into an Accumulo data store and request it
     * @throws ParseException probably
     * @throws IOException probably
     * @throws CQLException probably
     */
    public void testData() throws ParseException, IOException, CQLException {
        GeoMesaToolBox gm_tools = AccumuloGeoMesaToolBox.get(this.args);
        DataSet data = new CSVDataSet(
                "src/main/resources/test.csv", false,
                false, 4
        );

        DataStore datastore = gm_tools.addDataSet(data);
        Query q = new Query(data.getType(), ECQL.toFilter("0 = 0"));
        List<SimpleFeature> result;

        // Load the data and request it
        result = gm_tools.request(datastore, q);
        assertEquals(5, result.size());
    }
}
