package com.sword.eviegas.geomesa;

import java.io.IOException;
import java.util.List;

import org.apache.commons.cli.ParseException;
import org.geotools.data.DataStore;
import org.geotools.data.Query;
import org.geotools.filter.text.cql2.CQLException;
import org.geotools.filter.text.ecql.ECQL;
import org.opengis.feature.simple.SimpleFeature;

import com.sword.eviegas.data.CSVDataSet;
import com.sword.eviegas.data.DataSet;
import redis.clients.jedis.Jedis;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for any kind of data sets.
 */
public class GeoMesaToolBoxTest extends TestCase {
	/**
	 * Fake arguments from the console
	 */
	private String[] args;
	
    /**
     * Create the test case
     * @param testName name of the test case
     */
    public GeoMesaToolBoxTest(String testName) {
        super(testName);
        this.args = new String[4];
        this.args[0] = "--redis.url";
        this.args[1] = "localhost:6379";
        this.args[2] = "--redis.catalog";
        this.args[3] = "geomesa" ;
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(GeoMesaToolBoxTest.class);
    }

    /**
     * Test to create a data store instance
     * @throws ParseException probably
     * @throws IOException probably
     */
    public void testCreateDataStore() throws ParseException, IOException {
    	GeoMesaToolBox gm_tools = RedisGeoMesaToolBox.get(this.args);
    	DataStore datastore = gm_tools.createDataStore();
    	assertNotNull(datastore);
    }
    
    /**
     * Test to ensure the data schema
     * @throws ParseException probably
     * @throws IOException probably
     */
    public void testEnsureSchema() throws ParseException, IOException {
    	GeoMesaToolBox gm_tools = RedisGeoMesaToolBox.get(this.args);
    	DataStore datastore = gm_tools.createDataStore();
    	
    	// Data set with the writing rights
    	DataSet ds1 = new CSVDataSet(
    			"src/main/resources/test.csv", false,
				false, 4, "schema"
		);
    	gm_tools.ensureSchema(datastore, ds1);
    	assertNotNull(datastore.getSchema(ds1.getType()));
    	
    	// Read-only data set with an existed type
    	DataSet ds2 = new CSVDataSet(
    			"src/main/resources/test.csv", true,
				false, 4, "schema"
		);
    	gm_tools.ensureSchema(datastore, ds2);
    	assertNotNull(datastore.getSchema(ds2.getType()));
    	
    	// Read-only data set without any existed type
    	DataSet ds3 = new CSVDataSet(
    			"src/main/resources/test.csv", true,
				false, 4, "fake"
		);
    	
    	try {
    		gm_tools.ensureSchema(datastore, ds3);
		} catch (IllegalStateException e) {
			assertNull(datastore.getSchema(ds3.getType()));
		}
    }
    
    /**
     * Test to write features in the data store
     * The method 'addDataSet' will be also tested
     * @throws IOException probably
     * @throws ParseException probably
     */
    public void testWriteFeaturesAddDataSet()
    		throws ParseException, IOException {
    	GeoMesaToolBox gm_tools = RedisGeoMesaToolBox.get(this.args);
    	DataSet data = new CSVDataSet(
    			"src/main/resources/test.csv", false,
				false, 4
		);
    	DataStore datastore;
    	
    	data.load();
    	datastore = gm_tools.addDataSet(data);
    	assertNotNull(datastore);
    }
    
    /**
     * Test to set the parameters
     * @throws ParseException probably
     */
    public void testSet() throws ParseException {
    	GeoMesaToolBox gm_tools = RedisGeoMesaToolBox.get(this.args);    	
    	this.args[3] = "new-catalog";
    	gm_tools.set(this.args);
    }
    
    /**
     * Test to request a data store
     * @throws IOException probably
     * @throws ParseException probably
     * @throws CQLException probably
     */
    public void testRequest()
    		throws IOException, ParseException, CQLException {
    	Jedis jedis = new Jedis("localhost");
    	jedis.flushAll();
    	jedis.close();
    	
    	GeoMesaToolBox gm_tools = RedisGeoMesaToolBox.get(this.args);
    	DataSet data = new CSVDataSet(
    			"src/main/resources/test.csv", false,
				false, 4
		);

    	DataStore datastore = gm_tools.addDataSet(data);
    	Query q = new Query(data.getType(), ECQL.toFilter("0 = 0"));
    	List<SimpleFeature> result;
    	
    	// Load the data and request it
    	result = gm_tools.request(datastore, q);
    	assertEquals(5, result.size());
    }
    
    /**
     * Test to clean up the data store
     * @throws IOException probably
     * @throws ParseException probably
     */
    public void testCleanUp() throws ParseException, IOException {
    	GeoMesaToolBox gm_tools = RedisGeoMesaToolBox.get(this.args);
    	DataSet data = new CSVDataSet(
    			"src/main/resources/test.csv", false,
				false, 4
		);
    	DataStore datastore = gm_tools.addDataSet(data);
    	data.load();
    	gm_tools.cleanup(datastore, data);
    }
}
