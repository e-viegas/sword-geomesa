package com.sword.eviegas.geomesa;

import java.io.IOException;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.geotools.data.DataAccessFactory.Param;
import org.locationtech.geomesa.redis.data.RedisDataStoreFactory;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for the command lines.
 */
public class CommandLineTest extends TestCase {	
    /**
     * Create the test case
     * @param testName name of the test case
     */
    public CommandLineTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(CommandLineTest.class);
    }

    /**
     * Test to parse the arguments 
     * @throws IOException 
     */
    public void testParseArgs() throws IOException {
    	Param[] parameters = new RedisDataStoreFactory().getParametersInfo();
    	Options options = CommandLineDataStore.createOptions(parameters);
    	
    	options.addOption(Option.builder()
        		.longOpt("cleanup")
        		.desc("Delete tables after running")
        		.build()
        );
    	
    	// Normally, without any options, there is an error
    	try {
			CommandLineDataStore.parseArgs(
					GeoMesaToolBox.class,
					options,
					new String[0]
			);
		} catch (ParseException e) {
			assertTrue(true);
		}
    }
}
