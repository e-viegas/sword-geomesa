package com.sword.eviegas.geomesa;

import java.io.IOException;

import org.apache.commons.cli.ParseException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for any kind of data sets.
 */
public class RedisGeoMesaToolBoxTest extends TestCase {
    /**
     * Create the test case
     * @param testName name of the test case
     */
    public RedisGeoMesaToolBoxTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(RedisGeoMesaToolBoxTest.class);
    }

    /**
     * Test to get a tool box for Redis 
     * @throws ParseException 
     * @throws IOException 
     */
    public void testGet() throws ParseException, IOException {
    	String args[] = {
    		"--redis.url", "localhost:6379",
    		"--redis.catalog", "geomesa"
    	};
    	
    	GeoMesaToolBox gm_tools_1 = RedisGeoMesaToolBox.get(args);
    	GeoMesaToolBox gm_tools_2 = RedisGeoMesaToolBox.get(args);
    	assertEquals(gm_tools_1, gm_tools_2);
    }
}
