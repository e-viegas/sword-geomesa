package com.sword.eviegas.geomesa;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;
import org.geotools.data.DataStore;
import org.geotools.data.DataAccessFactory.Param;
import org.locationtech.geomesa.accumulo.data.AccumuloDataStoreFactory;
import org.locationtech.geomesa.accumulo.data.AccumuloDataStoreParams;
import org.locationtech.geomesa.security.AuthorizationsProvider;
import org.locationtech.geomesa.security.DefaultAuthorizationsProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Singleton to use the technology 'GeoMesa' with the data store 'Redis'
 * @author Erwan Viegas
 */
public class AccumuloGeoMesaToolBox extends GeoMesaToolBoxAbstract {
	/**
	 * Unique instance of GeoMesa Tool Box
	 */
	private static AccumuloGeoMesaToolBox INSTANCE = null;
	
	/**
	 * Constructor of the unique GeoMesa Tool Box
	 * This private constructor is called by the static method 'get()'
	 * @param args Accumulo parameters
	 * @throws ParseException When one of the different parameters are corrupted
	 */
	private AccumuloGeoMesaToolBox(String[] args)
			throws ParseException {
		super(
				new AccumuloDataStoreFactory().getParametersInfo(),
				args, AccumuloGeoMesaToolBox.options());
	}
	
	/**
	 * Get the unique instance of GeoMesa Tool Box (SINGLETON)
	 * @param args Accumulo parameters
	 * @return GeoMesa Tool Box
	 * @throws ParseException When one of the different parameters are corrupted
	 */
	public static AccumuloGeoMesaToolBox get(String[] args)
			throws ParseException {
		// Initialize the GeoMesa Tool Box if it does not exist
		if (AccumuloGeoMesaToolBox.INSTANCE == null) {
			AccumuloGeoMesaToolBox.INSTANCE = new AccumuloGeoMesaToolBox(args);
		}
		
		return AccumuloGeoMesaToolBox.INSTANCE;
	}

	/**
	 * Get a data store to stack the data
	 * @return A data store instance according to these parameters
	 * @throws IOException The data store could not be created. return = null
	 */
	@Override
	public DataStore createDataStore()
			throws IOException {
		// get an instance of the data store that uses the default
		// authorizations provider, which will use whatever auths the connector
		// has available
		System.setProperty(
				AuthorizationsProvider.AUTH_PROVIDER_SYS_PROPERTY,
				DefaultAuthorizationsProvider.class.getName()
		);

		return super.createDataStore();
	}

	/**
	 * Get the specific options for Accumulo authorization system
	 * @return Option list for the visibilities and authorizations
	 */
	private static List<Option> options() {
		List<Option> lst = new ArrayList<Option>();
		Param visibility = AccumuloDataStoreParams.VisibilitiesParam();
		Param authorizations = AccumuloDataStoreParams.AuthsParam();

		// Visibilities
		lst.add(Option.builder(null)
				.longOpt(visibility.getName())
				.argName(visibility.getName())
				.hasArg()
				.desc(visibility.getDescription().toString())
				.required(true)
				.build()
		);

		// Authorizations
		lst.add(Option.builder(null)
				.longOpt(authorizations.getName())
				.argName(authorizations.getName())
				.hasArg()
				.desc(authorizations.getDescription().toString())
				.required(true)
				.build()
		);

		return lst;
	}
}
