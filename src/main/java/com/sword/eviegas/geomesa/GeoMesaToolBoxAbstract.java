package com.sword.eviegas.geomesa;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.geotools.data.DataAccessFactory.Param;
import org.geotools.factory.Hints;
import org.geotools.filter.identity.FeatureIdImpl;
import org.locationtech.geomesa.index.geotools.GeoMesaDataStore;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.FeatureReader;
import org.geotools.data.FeatureWriter;
import org.geotools.data.Query;
import org.geotools.data.Transaction;

import com.sword.eviegas.data.DataSet;

/**
 * Singleton to use tools for 'GeoMesa'
 * @author Erwan Viegas
 */
public abstract class GeoMesaToolBoxAbstract implements GeoMesaToolBox {
	/**
	 * Parameters for the data store used
	 */
	private Map<String, String> parameters;
	
	/**
	 * Specific options for this kind of data store
	 */
	private final Options options;
	
	/**
	 * Constructor of the unique GeoMesa Tool Box
	 * This private constructor is called by the static method 'get()'
	 * 
	 * @param parameters Information about the data store type used
	 * @param args 		 Parameters for this specific data store type
	 * @throws ParseException When one of the different parameters are corrupted
	 */
	public GeoMesaToolBoxAbstract(Param[] parameters, String[] args)
			throws ParseException {
        this.options = CommandLineDataStore.createOptions(parameters);        
        this.set(args);
	}

	/**
	 * Constructor of the unique GeoMesa Tool Box
	 * This private constructor is called by the static method 'get()'
	 *
	 * @param parameters Information about the data store type used
	 * @param args 		 Parameters for this specific data store type
	 * @param options 	 Moree options to add
	 * @throws ParseException When one of the different parameters are corrupted
	 */
	public GeoMesaToolBoxAbstract(Param[] parameters, String[] args,
								  List<Option> options)
			throws ParseException {
		this.options = CommandLineDataStore.createOptions(parameters);

		for (Option opt : options) {
			this.options.addOption(opt);
		}

		this.set(args);
	}
	
	/**
	 * Get a data store to stack the data
	 * @return A data store instance according to these parameters
	 * @throws IOException The data store could not be created. return = null
	 */
	public DataStore createDataStore() throws IOException {
        return DataStoreFinder.getDataStore(this.parameters);
    }
	
	/**
	 * Ensure a schema for the data in this data set
	 * @param datastore Data store where the data schema will be put
	 * @param data Data set set, from which the schema comes
	 * @throws IOException If the data store is corrupted
	 */
	public void ensureSchema(DataStore datastore, DataSet data)
			throws IOException {
		SimpleFeatureType sft;
		
		if (data.isReadonly()) {
			// Read-only data set
			sft = datastore.getSchema(data.getType());
	        
	        if (sft == null) {
	            throw new IllegalStateException(
	            		"Schema '" + data.getType() +
	            		"' does not exist."
	            );
	        }
		} else {
			// Update the schema
			datastore.createSchema(data.getSimpleFeatureType());
		}
    }
	
	/**
	 * Put the features into the data store 
	 * @param datastore Data store, into which the features are put
	 * @param data Data set set, from which the features come
	 * @throws IOException If the data could not be written in the data store
	 */
	public void writeFeatures(
			DataStore datastore, DataSet data) throws IOException {
		
		// Create a feature writer
		String type = data.getType();		
		FeatureWriter<SimpleFeatureType, SimpleFeature> writer =
			datastore.getFeatureWriterAppend(type, Transaction.AUTO_COMMIT);
		SimpleFeature feature;
		SimpleFeature toWrite;
		
		// For each feature in the data set
		for (int k = 0; k < data.size(); k ++) {
			feature = data.getData(k);
			toWrite = writer.next();
			
			// copy the attribute names
			toWrite.setAttributes(feature.getAttributes());
			
			// copy the ID
			((FeatureIdImpl) toWrite.getIdentifier()).setID(feature.getID());
			toWrite.getUserData().put(Hints.USE_PROVIDED_FID, Boolean.TRUE);
			
			toWrite.getUserData().putAll(feature.getUserData());
			writer.write();
		}
		
		// Close the writer to commit the changes
		writer.close();
	}
	
	/**
	 * Add a new data set
	 * @param data Data set to add in the data store
	 * @return Data store with data | null=Error
	 * @throws IOException When the data can not be written into the store
	 */
	public DataStore addDataSet(DataSet data) throws IOException {
		DataStore datastore = this.createDataStore();
		this.ensureSchema(datastore, data);
		
		// No read-only => Write the features
		if (!data.isReadonly()) {
			data.load();
			this.writeFeatures(datastore, data);
		}
			
		return datastore;
	}
	
	/**
	 * Set the parameters for another data store
	 * @param args Parameters, like the URL address
	 * @throws ParseException If the options could not be parsed
	 */
	public void set(String[] args) throws ParseException {
        CommandLine command = CommandLineDataStore.parseArgs(
        		getClass(), this.options, args
        );
        this.parameters = CommandLineDataStore.getDataStoreParams(
        		command, this.options
        );
	}
	
	/**
	 * Run a query on a data store
	 * @param datastore Data store, on which the query will be run
	 * @param q Query to run on the data store
	 * @return List of features, returned by the query
	 * @throws IOException When the data store could not be read
	 */
	public List<SimpleFeature> request(DataStore datastore, Query q)
			throws IOException {
		FeatureReader<SimpleFeatureType, SimpleFeature> reader =
                datastore.getFeatureReader(q, Transaction.AUTO_COMMIT);
		List<SimpleFeature> result = new ArrayList<SimpleFeature>();
		
		// Fetch all features from the reader
		while (reader.hasNext()) {
			result.add(reader.next());
		}
		
		return result;
	}
	
	/**
	 * Clean up the data store
	 * @param datastore Data store to clean up
	 * @param data Data set to remove
	 */
	@SuppressWarnings("rawtypes")
	// TODO Suppress Warnings ?? cleanup:204
	public void cleanup(DataStore datastore, DataSet data) {
//		String type_name = data.getType();
		
        try {
        	((GeoMesaDataStore) datastore).delete();
        	// TODO no-GeoMesaDataStore instance
//                ((SimpleFeatureStore) datastore.getFeatureSource(
//                		type_name
//                	)).removeFeatures(Filter.INCLUDE);
//                datastore.removeSchema(type_name);
        } catch (Exception e) {
            System.err.println("Exception cleaning up test data: " + e.toString());
        } finally {
            // make sure that we dispose of the datastore when we're done with it
            datastore.dispose();
        }
	}
}
