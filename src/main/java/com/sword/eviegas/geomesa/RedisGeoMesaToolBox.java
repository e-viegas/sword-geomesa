package com.sword.eviegas.geomesa;

import org.apache.commons.cli.ParseException;
import org.locationtech.geomesa.redis.data.RedisDataStoreFactory;

/**
 * Singleton to use the technology 'GeoMesa' with the data store 'Redis'
 * @author Erwan Viegas
 */
public class RedisGeoMesaToolBox extends GeoMesaToolBoxAbstract {
	/**
	 * Unique instance of GeoMesa Tool Box
	 */
	private static RedisGeoMesaToolBox INSTANCE = null;
	
	/**
	 * Constructor of the unique GeoMesa Tool Box
	 * This private constructor is called by the static method 'get()'
	 * @param args Redis server address (e.g. localhost:6379)
	 * 	and the catalog name
	 * @throws ParseException When one of the different parameters are corrupted
	 */
	private RedisGeoMesaToolBox(String[] args)
			throws ParseException {
		super(new RedisDataStoreFactory().getParametersInfo(), args);
	}
	
	/**
	 * Get the unique instance of GeoMesa Tool Box (SINGLETON)
	 * @param args Redis server address (e.g. localhost:6379)
	 * 	and the catalog name
	 * @return GeoMesa Tool Box
	 * @throws ParseException When one of the different parameters are corrupted
	 */
	public static RedisGeoMesaToolBox get(String[] args)
			throws ParseException {
		// Initialize the GeoMesa Tool Box if it does not exist
		if (RedisGeoMesaToolBox.INSTANCE == null) {
			RedisGeoMesaToolBox.INSTANCE = new RedisGeoMesaToolBox(args);
		}
		
		return RedisGeoMesaToolBox.INSTANCE;
	}
}
