package com.sword.eviegas.geomesa;

import java.io.IOException;
import java.util.List;

import org.apache.commons.cli.ParseException;
import org.geotools.data.DataStore;
import org.geotools.data.Query;
import org.opengis.feature.simple.SimpleFeature;

import com.sword.eviegas.data.DataSet;

/**
 * Interface to use tools for 'GeoMesa'
 * @author Erwan Viegas
 */
public interface GeoMesaToolBox {
	/**
	 * Get a data store to stack the data
	 * @return A data store instance according to these parameters
	 * @throws IOException The data store could not be created. return = null
	 */
	DataStore createDataStore() throws IOException;
	
	/**
	 * Ensure a schema for the data in this data set
	 * @param datastore Data store where the data schema will be put
	 * @param data Data set set, from which the schema comes
	 * @throws IOException If the data store is corrupted
	 */
	void ensureSchema(DataStore datastore, DataSet data)
			throws IOException;
	
	/**
	 * Put the features into the data store 
	 * @param datastore Data store, into which the features are put
	 * @param data Data set set, from which the features come
	 * @throws IOException If the data could not be written in the data store
	 */
	void writeFeatures(DataStore datastore, DataSet data) throws IOException;
	
	/**
	 * Add a new data set
	 * @param data Data set to add in the data store
	 * @return Data store with data | null=Error
	 * @throws IOException When the data can not be written into the store
	 */
	DataStore addDataSet(DataSet data) throws IOException;
	
	/**
	 * Set the parameters for another data store
	 * @param args Parameters, like the URL address
	 * @throws ParseException If the options could not be parsed
	 */
	void set(String[] args) throws ParseException;
	
	/**
	 * Run a query on a data store
	 * @param datastore Data store, on which the query will be run
	 * @param q Query to run on the data store
	 * @return List of features, returned by the query
	 * @throws IOException When the data store could not be read
	 */
	List<SimpleFeature> request(DataStore datastore, Query q)
			throws IOException;
	
	/**
	 * Clean up the data store
	 * @param datastore Data store to clean up
	 * @param data Data set to remove
	 */
	void cleanup(DataStore datastore, DataSet data);
}
