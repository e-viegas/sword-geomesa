package com.sword.eviegas.data;

import java.io.IOException;
import java.util.List;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

/**
 * Common code source for the data sets
 * @author Erwan Viegas
 */
public abstract class DataSetAbstract implements DataSet {
	/**
	 * Type of a simple feature of the data set
	 */
	protected SimpleFeatureType sft;
	
	/**
	 * All features of the data set
	 */
	protected List<SimpleFeature> features;
	
	/**
	 * URL where there is the resource to load
	 */
	protected String url;
	
	/**
	 * Flag for read-only mode
	 */
	private final boolean readonly;
	
	/**
	 * Build a new instance to load a data set
	 * @param url Address for this data set
	 * @param readonly Flag for read-only mode
	 */
	public DataSetAbstract(String url, boolean readonly) {
		this.sft = null;
		this.features = null;
		this.url = url;
		this.readonly = readonly;
	}

	/**
	 * Get the type of this data set
	 * @return Type as a string
	 */
	public abstract String getType();

	/**
	 * Get the type of a simple feature stacked in this data set
	 * @return SimpleFeatureType instance
	 * @throws IOException If the file not found
	 */
	public abstract SimpleFeatureType getSimpleFeatureType() throws IOException;

	/**
	 * Load the data from a file
	 *
	 * @throws IOException If the file is not found
	 */
	public void load() throws IOException {
		this.load(null);
	}

	/**
	 * Load the data from a file
	 * @param visibilities Rights to access to this data
	 * @throws IOException If the file is not found
	 */
	public abstract void load(String visibilities) throws IOException;
	
	/**
	 * Get the data inside this data set
	 * @return List of features
	 */
	public SimpleFeature getData(int k) {
		if (this.features == null) {
			return null;
		} else if (0 <= k && k < this.features.size()) {
			return this.features.get(k);
		} else {
			return null;
		}
	}

	/**
	 * Get the number of features in this data set
	 * @return Data size as an integer
	 */
	public int size() {
		if (this.features == null) {
			return -1;
		} else {
			return this.features.size();
		}
	}

	/**
	 * Check if the data store is on read-only mode
	 * @return TRUE for read-only
	 */
	public boolean isReadonly() {
		return readonly;
	}
}
