package com.sword.eviegas.data;

import java.io.IOException;

import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

/**
 * Interface for the data sets
 * @author Erwan Viegas
 */
public interface DataSet {
    /**
     * Get the type of this data set
     * @return Type as a string
     */
    String getType();

    /**
     * Get the type of a simple feature stacked in this data set
     * @return SimpleFeatureType instance
     * @throws IOException If the file is not found
     */
    SimpleFeatureType getSimpleFeatureType() throws IOException;

    /**
     * Load the data from a file
     * @throws IOException If the file is not found
     */
    void load() throws IOException;

    /**
     * Load the data from a file
     * @param visibilities Rights to access to this data
     * @throws IOException If the file is not found
     */
    void load(String visibilities) throws IOException;

    /**
     * Get the data inside this data set
     * @param k index of the feature wanted
     * @return Feature a this index
     */
    SimpleFeature getData(int k);

    /**
     * Get the number of features in this data set
     * @return Data size as an integer
     */
    int size();

    /**
     * Check if the data store is on read-only mode
     * @return TRUE for read-only
     */
    boolean isReadonly();
}
