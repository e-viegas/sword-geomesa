package com.sword.eviegas.data;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.geotools.factory.Hints;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.locationtech.geomesa.utils.interop.SimpleFeatureTypes;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static org.apache.commons.csv.CSVFormat.newFormat;

/**
 * Data set from a CSV file
 * @author Erwan Viegas
 */
public class CSVDataSet extends DataSetAbstract {
	/**
	 * REGEX to check if the format is WKT
	 */
	private static final String regexWkt = "^POINT( )*\\(( )*[-+]?(180|1[0-7]" +
			"\\d\\.?\\d*|\\d{1,2}\\.?\\d*)[ ]+[-+]?(90|[0-8]?\\d\\.?\\d*)( )*" +
			"\\)$";
	
	/**
	 * Pattern to detect a WKT format
	 */
	private static final Pattern p = Pattern.compile(CSVDataSet.regexWkt);
	
	/**
	 * Type of this file. Default: CSV-file
	 */
	private String type;
	
	/**
	 * Different types created here
	 */
	private static Map<String, Integer> types = new HashMap<String, Integer>();
	
	/**
	 * Fields of CSV file to load
	 */
	private List<String> fields;
	
	/**
	 * To know if there is a header line
	 */
	private boolean header;
	
	/**
	 * Column index for geometry
	 */
	private int geom;
	
	/**
	 * Build a new instance to load a CSV file
	 * @param url Address for this CSV file
	 * @param readonly Flag for read-only mode
	 * @param header Flag for the header TRUE=read-only
	 * @param geom Index of the column used for the geometry
	 */
	public CSVDataSet(String url, boolean readonly, boolean header,
			int geom) {
		this(url, readonly, header, geom, "file");
	}
	
	/**
	 * Build a new instance to load a CSV file with a type name
	 * @param url Address for this CSV file
	 * @param readonly Flag for read-only mode
	 * @param header Flag for the header
	 * @param geom Index of the column used for the geometry
	 * @param type Specific type name
	 */
	public CSVDataSet(String url, boolean readonly, boolean header,
			int geom, String type) {		
		super(url, readonly);
		int counter;
		
		this.fields = new ArrayList<String>();
		this.header = header;
		this.geom = geom;
		
		if (this.isReadonly()) {
			this.type = "CSV-" + type;
		} else if (CSVDataSet.types.containsKey(type)) {
			/* Already existed */
			counter = CSVDataSet.types.get(type) + 1;
			CSVDataSet.types.put(type, counter);
			this.type = "CSV-" + type + "-" + counter;
		} else {
			/* New type */
			CSVDataSet.types.put(type, 0);
			this.type = "CSV-" + type;
		}
	}

	/**
	 * Get the type of this data set
	 * @return Type as a string
	 */
	@Override
	public String getType() {
		return this.type;
	}

	/**
	 * Get the type of a simple feature stacked in this data set
	 * @return SimpleFeatureType instance
	 * @throws IOException If the file is not found
	 */
	@Override
	public SimpleFeatureType getSimpleFeatureType() throws IOException {
		if (this.sft == null) {
			// If the simple feature type has not been set yet
			SimpleFeatureType sft;
		    StringBuilder attr = new StringBuilder();
		    this.readHeader();
		  
		    // Add the attributes
			for (String field : this.fields) {
				attr.append(field);
				attr.append(":String,");
			}
		    
		    // Add the geometry and create the new type
		    attr.append("*geom:Point:srid=4326");
		    sft = SimpleFeatureTypes.createType(
		    		this.getType(), attr.toString()
		    );
			
		    // Choose a field for the date
            sft.getUserData().put(SimpleFeatureTypes.DEFAULT_DATE_KEY, "dt");
			this.sft = sft;
		}
		
		// Return the simple feature type
		return this.sft;
	}

	/**
	 * Get the data inside this data set
	 * @throws IOException If the file is not found
	 */
	@Override
	public void load(String visibilities) throws IOException {
		File file = new File(this.url);
		boolean onHeader = this.header;

		// Parser to read the CSV file
		CSVParser parser = CSVParser.parse(
				file, Charset.forName("UTF8"), newFormat(';')
		);
		
		// Builder to build a feature attribute by attribute
        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(
        		this.getSimpleFeatureType()
        );
        
        // The features has been already loaded => skip !
 		if (this.features != null)
 			return;
        
        // Feature list, where the new feature are stacked
        this.features = new ArrayList<SimpleFeature>();
        
        // Add each feature from the CSV file to the feature list
        for (CSVRecord record : parser) {
        	if (onHeader) {
        		// Skip the header if it exists
        		onHeader = false;
			} else {
				this.toFeature(builder, record, visibilities);
			}
		}
	}
	
	/**
	 * Get the fields 
	 * @return Field list
	 */
	List<String> getFields(){
		return this.fields;
	}
	
	/**
	 * Read the header and fill the attribute 'fields'
	 * @throws IOException If the file is not found
	 */
	private void readHeader() throws IOException {
		File file = new File(this.url);
		CSVParser parser = CSVParser.parse(
				file, Charset.forName("UTF8"), newFormat(';')
		);
		List<CSVRecord> lst = parser.getRecords();
		CSVRecord record;
		int n;
		
		// Read the first record
		record = lst.get(0);
		
		// Number of columns for this record
		// We consider, all records from a file has the same number of columns
		n = record.size();
		
		if (this.header) {
			// Read the header
			for (int k = 0; k < n; k ++) {
				this.fields.add(encode(record.get(k)));
			}
		} else {
			// No header -> Name by default
			for (int k = 0; k < n; k ++) {
				this.fields.add("fld" + k);
			}
		}
	}
	
	/**
	 * Encode a string
	 * @param str String to encode
	 * @return String encoded
	 */
	private static String encode(String str) {
		String res;
		res = str.replace(" ", "\\x20");
		res = res.replace(":", "\\x3A");
		res = res.replace("*", "\\x2A");
		return res;
	}
	
	/**
	 * Convert a CSV record into a feature for GeoMesa with a geometry
	 * @param builder Feature builder with the adequate simple-feature type
	 * @param record  Record to convert
	 * @param visibilities Rights to access to this data
	 */
	private void toFeature(SimpleFeatureBuilder builder, CSVRecord record,
						   String visibilities) {
		SimpleFeature feature;
    	
		// Get the value of each field
    	for (int k = 0; k < this.fields.size(); k ++) {
    		try {
				builder.set(this.fields.get(k), record.get(k));
			} catch (Exception e) {
				builder.set(this.fields.get(k), "");
			}
		}
    	
    	// Build the geometry
    	if (this.geom < record.size() &&
    			CSVDataSet.p.matcher(record.get(this.geom)).find()) {
    		builder.set("geom", record.get(this.geom));
		} else {
			builder.set("geom", "POINT (0 0)");
		}
    	
    	// Add the new feature to the list and reset the builder
    	builder.featureUserData(Hints.USE_PROVIDED_FID, true);
    	feature = builder.buildFeature(null);

    	if (visibilities != null) {
			feature.setAttribute("visibility", visibilities);
		}

    	this.features.add(feature);
    	builder.reset();
	}
}
