package com.sword.eviegas.app;

import com.sword.eviegas.data.CSVDataSet;
import com.sword.eviegas.data.DataSet;
import com.sword.eviegas.geomesa.GeoMesaToolBox;
import com.sword.eviegas.geomesa.RedisGeoMesaToolBox;
import org.apache.commons.cli.ParseException;

import java.io.IOException;

/**
 * Main application
 * @author Erwan Viegas
 */
public class App {
    public static void main(String[] args) throws ParseException, IOException {
        DataSet data = new CSVDataSet(
                "src/main/resources/test.csv",
                false,
                false,
                4
        );

        GeoMesaToolBox gm_tool = RedisGeoMesaToolBox.get(args);
        gm_tool.addDataSet(data);
    }
}